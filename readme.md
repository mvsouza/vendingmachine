# Vending machine Kata

This kata is intented to exercise your developing skills and show what you are capable of.
Some interesting topics to you to considered:

  - Object Oriented Design (class, abstract, Interface, override, access permissions, inheritance, polymorphism);
  - C# and dotnet features (extension method, reflection, dynamic, lambda, delegate, Linq, C# 7, Exception Handling, generic, Variance/Covariance, Task, await, async);
  - Agile patterns, principles and practices in Agile (Craftsmanship);
  - Solution architecture and derivablity; 
  - Testability and quality strategies;
  - Documentation capability: it should be easy to understand your desing/architecture choises and how it should function;
  - We like web applications, cosider doing one, UX isn't a issue here;
  - Write this application as you would deliver to production, not as a demo;

## User Stories

These user stories are the Katas backbone. Please complement the acceptance criteria as you see fit.

***As*** a buyer 
***I would like to*** insert a coin 
***In order to*** raise my credit values.

  - Should have the follwing coins available:
    - 0.01, 0.05, 0.10, 0.25, 0.50, 1.00


***As*** a buyer 
***I would like to*** see how much credit I have so far
***In order to*** buy some product

  - The machine starts with 10 units of each coin

***As*** a buyer 
***I would like to*** chose a product, 
***In order to*** purchase it

  - It has on stock Coke for 1.50, Water for 1.00 and Pastelina for 0.30

***As*** a logistics handler 
***I would like to*** update the machines stock

  - The machine starts with 10 units of each product

***As*** a logistics handler 
***I would like to*** update the machines cash

***As*** a retailer 
***I would like to*** know the stock available


